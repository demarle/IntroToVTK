#include "vtkActor.h"
#include "vtkCommand.h"
#include "vtkContourFilter.h"
#include "vtkImageActor.h"
#include "vtkImageData.h"
#include "vtkImageClip.h"
#include "vtkImageMapper3D.h"
#include "vtkInteractorStyleImage.h"
#include "vtkPolyDataMapper.h"
#include "vtkProperty.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkSmartPointer.h"
#include "vtkXMLImageDataReader.h"

// This is our callback which is a subclass of
// the vtkCommand class
class vtkMyCallback : public vtkCommand
{
public:
  // We need a new method
  static vtkMyCallback *New() {return new vtkMyCallback;};

  // Initialize ivars
  vtkMyCallback()
    {
      this->Interactor    = nullptr;
      this->RenderWindow  = nullptr;
      this->ImageActor    = nullptr;
      this->Renderer      = nullptr;
      this->Clip          = nullptr;
      this->MouseDown     = 0;
    }


  // Simple solution - just add simple, non-reference counted
  // set method for the ivars we need access to.
  // Set method for the interactor - we need it to
  // get the mouse position
  void SetInteractor( vtkRenderWindowInteractor *i )
    {this->Interactor = i;}

  // Set method for the actor - we need it to adjust
  // the slice being displayed
  void SetImageActor( vtkImageActor *a )
    {this->ImageActor = a;}

  // Set method for the render window - we need the size
  void SetRenderWindow( vtkRenderWindow *r )
    {this->RenderWindow = r;}

  // Set method for the renderer - need to reset clipping
  void SetRenderer( vtkRenderer *r )
    {this->Renderer = r;}

  // Set method for clip - we need it to adjust the
  // output whole extent
  void SetClip( vtkImageClip *c)
    {this->Clip = c;}

  // The execute method
  virtual void Execute (vtkObject *, unsigned long event, void *)
    {
      // Note that the mouse button was pressed
      if (event == vtkCommand::LeftButtonPressEvent)
	{
	this->MouseDown = 1;
	}
      // Note that the mouse button was released
      else if (event == vtkCommand::LeftButtonReleaseEvent)
	{
	this->MouseDown = 0;
	}
      // This is a mouse motion event
      else if (event == vtkCommand::MouseMoveEvent)
	{
	// If the mouse is currently down...
	if ( this->MouseDown )
	  {
	  // Get the last position of the interactor
	  int lastPos[2];
	  this->Interactor->GetLastEventPosition(lastPos);

	  // Get the size of the render window
	  int *size = this->RenderWindow->GetSize();

	  // Get the dimensions from the input to the image actor
	  int dim[3];
	  this->ImageActor->GetInput()->GetDimensions(dim);

	  // Figure out what slice we want to view - make it based on the
	  // y location as a percentage of height (map this to z image)
	  int newSlice = (double)(dim[2]-1)*(double)(lastPos[1])/(double)(size[1]);

	  // Make sure this is a valid number - remember, mouse positions outside the
	  // render window are possible. If it is OK, then change the OutputWholeExtent
	  // of the clip and the DisplayExtent of the image actor. You'll also need to
	  // call ResetCamearClippingRange on the renderer, and Render on the render window
	  // to see the change.
	  if ( newSlice >= 0 && newSlice < dim[2] )
	    {
	    this->ImageActor->SetDisplayExtent( 0, dim[0]-1, 0, dim[1]-1, newSlice, newSlice );
	    this->Clip->SetOutputWholeExtent( 0, dim[0]-1, 0, dim[1]-1, newSlice, newSlice );
	    this->Renderer->ResetCameraClippingRange();
	    this->RenderWindow->Render();
	    }

	  }
	// Otherwise, allow this motion to be handled by the style
	else
	  {
	  vtkInteractorStyle *style = vtkInteractorStyle::SafeDownCast(
	    this->Interactor->GetInteractorStyle());
	  if (style)
	    {
	    style->OnMouseMove();
	    }
	  }
	}
    }

private:
  vtkRenderWindowInteractor *Interactor;
  vtkRenderWindow           *RenderWindow;
  vtkRenderer               *Renderer;
  vtkImageActor             *ImageActor;
  vtkImageClip              *Clip;
  int                       MouseDown;
};

int main()
{
  // You'll need a render window to show it in
  vtkSmartPointer<vtkRenderWindow> renwin = vtkSmartPointer<vtkRenderWindow>::New();

  // You'll need a renderer in the render window
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();

  // Put in renderer in the render window
  renwin->AddRenderer(renderer);

  // Create the interactor
  vtkSmartPointer<vtkRenderWindowInteractor> interactor =
    vtkSmartPointer<vtkRenderWindowInteractor>::New();

  // Set the interactor on the render window
  renwin->SetInteractor(interactor);

  // You'll need to read the file using a vtkXMLImageDataReader
  vtkSmartPointer<vtkXMLImageDataReader> reader =
    vtkSmartPointer<vtkXMLImageDataReader>::New();

  // This is the file name.
  std::string filename = "../../../../head.vti";

  // Set the file name on the reader to filename.c_str()
  reader->SetFileName( filename.c_str() );

  // You'll need an image actor for display
  vtkSmartPointer<vtkImageActor> actor = vtkSmartPointer<vtkImageActor>::New();


  // Put the image actor in the renderer
  renderer->AddActor(actor);

  // Tell the image actor what to display - first we'll want to
  // find out the whole extent of the reader's output
  reader->Update();
  int extent[6];
  reader->GetOutput()->GetExtent(extent);


  // We need a vtkImageClip
  vtkSmartPointer<vtkImageClip> clip = vtkSmartPointer<vtkImageClip>::New();

  // Connect the output of the reader to the clip filter,
  // then set the OutputWholeExtent to match the DisplayExtent of
  // the image actor
  clip->SetInputConnection(reader->GetOutputPort());
  clip->SetOutputWholeExtent(extent[0], extent[1], extent[2], extent[3],
			   (extent[4]+extent[5])/2,
			   (extent[4]+extent[5])/2 );
  // Connect the reader to the image actor
  actor->GetMapper()->SetInputConnection(clip->GetOutputPort());

  // Create a vtkContourFilter
  vtkSmartPointer<vtkContourFilter> contour = vtkSmartPointer<vtkContourFilter>::New();

  // Set the input to the output of the clip
  // Use a contour value of 100
  contour->SetInputConnection(clip->GetOutputPort());
  contour->SetValue(0,100);

  // Create a vtkPolyDataMapper
  vtkSmartPointer<vtkPolyDataMapper> contourMapper = vtkSmartPointer<vtkPolyDataMapper>::New();

  // Set the input to the output of the contour filter
  // Turn of scalar visibility so we can color the line
  contourMapper->SetInputConnection(contour->GetOutputPort());
  contourMapper->ScalarVisibilityOff();

  // Create a vtkActor
  vtkSmartPointer<vtkActor> contourActor = vtkSmartPointer<vtkActor>::New();

  // Connect the mapper to the actor
  // Get the property and change the color to green
  contourActor->SetMapper(contourMapper);
  contourActor->GetProperty()->SetColor(0,1,0);

  // Add the contour actor to the renderer
  renderer->AddActor(contourActor);

  // Create the image interactor style and use it
  vtkSmartPointer<vtkInteractorStyleImage> style =
    vtkSmartPointer<vtkInteractorStyleImage>::New();
  interactor->SetInteractorStyle(style);

  // Create an instance of our callback
  vtkSmartPointer<vtkMyCallback> callback =
    vtkSmartPointer<vtkMyCallback>::New();

  // Connect the callback ivars
  callback->SetInteractor(interactor);
  callback->SetImageActor(actor);
  callback->SetRenderWindow(renwin);
  callback->SetRenderer(renderer);
  callback->SetClip(clip);

  // Add our own callback on mouse move
  style->AddObserver(vtkCommand::LeftButtonPressEvent, callback);
  style->AddObserver(vtkCommand::LeftButtonReleaseEvent, callback);
  style->AddObserver(vtkCommand::MouseMoveEvent, callback);

  // Render and start the interactor
  renwin->Render();
  interactor->Start();

  return 0;
}
