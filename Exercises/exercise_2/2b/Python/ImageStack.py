from vtk import *

# This is our callback which is a subclass of
# the vtkCommand class
def MyCallback(sender,event=""):
  # Note that the mouse button was pressed
  if event == "LeftButtonPressEvent":
    sender.MouseDown = 1
  # Note that the mouse button was released
  elif event == "LeftButtonReleaseEvent":
    sender.MouseDown = 0
  # This is a mouse motion event
  elif event == "MouseMoveEvent":
    # If the mouse is currently down...
    if sender.MouseDown==1:
      # Get the last position of the interactor
      lastPos = sender.interactor.GetLastEventPosition()

      # Get the size of the render window
      size = sender.renwin.GetSize()

      # Get the dimensions from the input to the image actor
      dim = sender.actor.GetInput().GetDimensions()

      # Figure out what slice we want to view - make it based on the
      # y location as a percentage of height (map this to z image)
      newSlice = (dim[2]-1)*(lastPos[1])/(size[1])

      # Make sure this is a valid number - remember, mouse positions outside the
      # render window are possible. If it is OK, then change the DisplayExtent of
      # the image actor. You'll also need to call ResetCamearClippingRange on the
      # renderer, and Render on the render window to see the change.
      if ( newSlice >= 0 and newSlice < dim[2] ):
        # todo: set actor's display extend and clip's outputwhole extent per the slice
	sender.renderer.ResetCameraClippingRange()
	sender.renwin.Render()
    # Otherwise, allow this motion to be handled by the style
    else:
      style = sender.interactor.GetInteractorStyle()
      style.OnMouseMove()

# You'll need a render window to show it in
renwin = vtkRenderWindow()

# You'll need a renderer in the render window
renderer = vtkRenderer()

# Put in renderer in the render window
renwin.AddRenderer(renderer)

# Create the interactor
interactor = vtkRenderWindowInteractor()

# Set the interactor on the render window
renwin.SetInteractor(interactor)

# You'll need to read the file using a vtkXMLImageDataReader
reader = vtkXMLImageDataReader()

# This is the file name.
filename = "../../head.vti"

# Set the file name on the reader to filename
reader.SetFileName( filename )
reader.Update()

# You'll need an image actor for display
actor = vtkImageActor()

# Connect the reader to the image actor
actor.SetInputData(reader.GetOutput())

# Put the image actor in the renderer
renderer.AddActor(actor)

# Tell the image actor what to display - first we'll want to
# find out the whole extent of the reader's output
reader.Update()
extent = reader.GetOutput().GetExtent()

# Now use the middle slice in z as the DisplayExtent for the
# image actor
actor.SetDisplayExtent( extent[0], extent[1], extent[2], extent[3],
 (extent[4]+extent[5])/2,
 (extent[4]+extent[5])/2 )

# We need a vtkImageClip
clip = vtkImageClip()

# Connect the output of the reader to the clip filter,
# then set the OutputWholeExtent to match the DisplayExtent of
# the image actor
clip.SetInputConnection(reader.GetOutputPort())
clip.SetOutputWholeExtent(extent[0], extent[1], extent[2], extent[3],
 (extent[4]+extent[5])/2,
 (extent[4]+extent[5])/2 )

# Create a vtkContourFilter
contour = vtkContourFilter()

# Set the input to the output of the clip
# Use a contour value of 100
contour.SetInputConnection(clip.GetOutputPort())
#contour.SetInputConnection(reader.GetOutputPort())
contour.SetValue(0,100)

# Create a vtkPolyDataMapper
contourMapper = vtkPolyDataMapper()

# Set the input to the output of the contour filter
# Turn of scalar visibility so we can color the line
contourMapper.SetInputConnection(contour.GetOutputPort())
contourMapper.ScalarVisibilityOff()

# Create a vtkActor
contourActor = vtkActor()

# Connect the mapper to the actor
# Get the property and change the color to green
contourActor.SetMapper(contourMapper)
contourActor.GetProperty().SetColor(0,1,0)

# Add the contour actor to the renderer
renderer.AddActor(contourActor)

# Create the image interactor style and use it
style = vtkInteractorStyleImage()
interactor.SetInteractorStyle(style)

# Connect the callback ivars
style.interactor = interactor
style.actor = actor
style.renwin = renwin
style.renderer = renderer
style.clip = clip
style.MouseDown = 0

# Add our own callback on mouse move
style.AddObserver("LeftButtonPressEvent", MyCallback)
style.AddObserver("LeftButtonReleaseEvent", MyCallback)
style.AddObserver("MouseMoveEvent", MyCallback)

# Render and start the interactor
renwin.Render()
interactor.Start()
