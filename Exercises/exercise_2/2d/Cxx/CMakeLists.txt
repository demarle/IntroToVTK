cmake_minimum_required(VERSION 2.8)

# I'm using the same script for all the exercises, so lets use a variable
set(EXERCISENAME "VolumeRender")
project("${EXERCISENAME}")

# Find VTK
find_package(VTK REQUIRED)

# make VTK's include directories available to the compiler
include(${VTK_USE_FILE})

# make an executable, we've only got one source file in this case
add_executable("${EXERCISENAME}" MACOSX_BUNDLE "${EXERCISENAME}.cxx")

# make VTK's libraries available to the linker
target_link_libraries("${EXERCISENAME}" ${VTK_LIBRARIES})
