#include <vtkActor.h>
#include <vtkColorTransferFunction.h>
#include <vtkContourFilter.h>
#include <vtkGPUVolumeRayCastMapper.h>
#include <vtkPiecewiseFunction.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkVolume.h>
#include <vtkVolumeProperty.h>
#include <vtkXMLImageDataReader.h>

int main(int, char* [])
{
  // You'll need a render window to show it in
  vtkSmartPointer<vtkRenderWindow> renwin = vtkSmartPointer<vtkRenderWindow>::New();

  // You'll need a renderer in the render window
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();

  // Put in renderer in the render window
  renwin->AddRenderer(renderer);

  // You'll need an interactor for interaction
  vtkSmartPointer<vtkRenderWindowInteractor> interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();

  // Set interactor on the render window
  renwin->SetInteractor(interactor);

  // You'll need to read the file using a vtkXMLImageDataReader
  vtkSmartPointer<vtkXMLImageDataReader> reader = vtkSmartPointer<vtkXMLImageDataReader>::New();

  // This is the file name
  const char *filename = "../../../head.vti";

  // Set the file name on the reader to filename
  reader->SetFileName( filename );

  // Create a vtkContourFilter to do the isocontouring
  // Set the input to the output of the reader
  // Set the value to 135
  vtkSmartPointer<vtkContourFilter> contour = vtkSmartPointer<vtkContourFilter>::New();
  contour->SetInputConnection(reader->GetOutputPort());
  contour->SetValue(0,135);

  // This is the vtkPolyDataMapper
  vtkSmartPointer<vtkPolyDataMapper> contourMapper = vtkSmartPointer<vtkPolyDataMapper>::New();

  // Connect the mapper to the contour filter
  // Remember to turn ScalarVisibilityOff()
  contourMapper->SetInputConnection(contour->GetOutputPort());
  contourMapper->ScalarVisibilityOff();

  // This is the vtkActor
  vtkSmartPointer<vtkActor> contourActor = vtkSmartPointer<vtkActor>::New();

  // Set the mapper
  contourActor->SetMapper(contourMapper);

  // Add the actor
  //renderer->AddActor(contourActor);

  // Create an opacity transfer function to map
  // scalar value to opacity
  vtkSmartPointer<vtkPiecewiseFunction> opacityFun = vtkSmartPointer<vtkPiecewiseFunction>::New();

  // todo: populate the opacity transfer function like so
  // Set a mapping going from 0.0 opacity at 90, up to 0.2 at 100,
  // and back down to 0.0 at 120.

  // Create a color transfer function for the mapping of scalar
  // value into color
  vtkSmartPointer<vtkColorTransferFunction> colorFun = vtkSmartPointer<vtkColorTransferFunction>::New();

  // Set the color to a constant value, you might
  // want to try (0.8, 0.4, 0.2)
  colorFun->AddRGBPoint(90.0, 0.8, 0.4, 0.2);
  colorFun->AddRGBPoint(150.0, 1, 1, 1);

  // Create a volume property
  // Set the opacity and color. Change interpolation
  // to linear for a more pleasing image
  vtkSmartPointer<vtkVolumeProperty> property = vtkSmartPointer<vtkVolumeProperty>::New();
  property->SetScalarOpacity(opacityFun);
  property->SetColor(colorFun);
  property->SetInterpolationTypeToLinear();

  // Create the GPU volume ray cast mapper
  vtkSmartPointer<vtkGPUVolumeRayCastMapper> mapper = vtkSmartPointer<vtkGPUVolumeRayCastMapper>::New();

  // Set the input to the output of the reader
  mapper->SetInputConnection(reader->GetOutputPort());

  // Create the volume
  vtkSmartPointer<vtkVolume> volume = vtkSmartPointer<vtkVolume>::New();

  // Set the property and the mapper
  volume->SetProperty(property);
  volume->SetMapper(mapper);

  // Add the volume to the renderer
  renderer->AddVolume(volume);

  // Render and start the interactor
  renwin->Render();
  interactor->Start();
}
