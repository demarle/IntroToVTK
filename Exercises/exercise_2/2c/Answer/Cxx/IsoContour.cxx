#include <vtkActor.h>
#include <vtkContourFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkXMLImageDataReader.h>

int main(int, char* [])
{
  // You'll need a render window to show it in
  vtkSmartPointer<vtkRenderWindow> renwin = vtkSmartPointer<vtkRenderWindow>::New();

  // You'll need a renderer in the render window
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();

  // Put in renderer in the render window
  renwin->AddRenderer(renderer);

  // You'll need an interactor for interaction
  vtkSmartPointer<vtkRenderWindowInteractor> interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();

  // Set interactor on the render window
  renwin->SetInteractor(interactor);

  // You'll need to read the file using a vtkXMLImageDataReader
  vtkSmartPointer<vtkXMLImageDataReader> reader = vtkSmartPointer<vtkXMLImageDataReader>::New();

  // This is the file name
  const char *filename = "../../../../head.vti";

  // Set the file name on the reader to filename
  reader->SetFileName( filename );

  // Create a vtkContourFilter to do the isocontouring
  // Set the input to the output of the reader
  // Set the value to 135
  vtkSmartPointer<vtkContourFilter> contour = vtkSmartPointer<vtkContourFilter>::New();
  contour->SetInputConnection(reader->GetOutputPort());
  contour->SetValue(0,135);

  // This is the vtkPolyDataMapper
  vtkSmartPointer<vtkPolyDataMapper> contourMapper = vtkSmartPointer<vtkPolyDataMapper>::New();

  // Connect the mapper to the contour filter
  // Remember to turn ScalarVisibilityOff()
  contourMapper->SetInputConnection(contour->GetOutputPort());
  contourMapper->ScalarVisibilityOff();

  // This is the vtkActor
  vtkSmartPointer<vtkActor> contourActor = vtkSmartPointer<vtkActor>::New();

  // Set the mapper
  contourActor->SetMapper(contourMapper);

  // Add the actor
  renderer->AddActor(contourActor);

  // Render and start the interactor
  renwin->Render();
  interactor->Start();
}
