from vtk import *

# You'll need to read the file
reader = vtkXMLImageDataReader()

# You'll need a render window to show it in
renwin = vtkRenderWindow()

# You'll need a renderer in the render window
renderer = vtkRenderer()

# This is the file name.
filename = "../../head.vti"

# Set the file name on the reader to filename.c_str()
reader.SetFileName(filename)

# Put in renderer in the render window
renwin.AddRenderer(renderer)

# Create the interactor
interactor =  vtkRenderWindowInteractor()

# Set the interactor on the render window
renwin.SetInteractor(interactor)

# todo: the following
# Create a vtkContourFilter to do the isocontouring
# Set the input to the output of the reader
# Set the value to 135

# This is the vtkPolyDataMapper
contourMapper = vtkPolyDataMapper()

# Connect the mapper to the contour filter
# Remember to turn ScalarVisibilityOff()
contourMapper.SetInputConnection(contour.GetOutputPort())
contourMapper.ScalarVisibilityOff()

# This is the vtkActor
contourActor = vtkActor()

# Set the mapper
contourActor.SetMapper(contourMapper)

# Add the actor
renderer.AddActor(contourActor)

# Render and start the interactor
renwin.Render()
interactor.Start()
