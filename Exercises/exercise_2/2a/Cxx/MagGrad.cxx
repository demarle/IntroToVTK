#include <vtkInteractorStyleImage.h>
#include <vtkImageActor.h>
#include <vtkImageData.h>
#include <vtkImageGradientMagnitude.h>
#include <vtkImageThreshold.h>
#include <vtkPointData.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkUnsignedCharArray.h>

#include <math.h>

int main(int, char* [])
{
  // You'll need a render window to show it in
  vtkSmartPointer<vtkRenderWindow> renwin = vtkSmartPointer<vtkRenderWindow>::New();

  // You'll need a renderer in the render window
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();

  // Put in renderer in the render window
  renwin->AddRenderer(renderer);

  // You'll need an interactor for interaction
  vtkSmartPointer<vtkRenderWindowInteractor> interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();

  // Set interactor on the render window
  renwin->SetInteractor(interactor);

  vtkSmartPointer<vtkInteractorStyleImage> style = vtkSmartPointer<vtkInteractorStyleImage>::New();
  interactor->SetInteractorStyle(style);

  // You'll need to create the unsigned char
  // array that will hold the data
  vtkSmartPointer<vtkUnsignedCharArray> data = vtkSmartPointer<vtkUnsignedCharArray>::New();
  data->SetNumberOfComponents(1);
  data->SetNumberOfValues(500*500);
  int index = 0;
  for (int i = 0; i<500; i++)
  {
    for (int j = 0; j<500; j++)
    {
      data->InsertValue(index,(int)(127.5 * (1.0 + sin(i/25.0) * cos(j/25.0))));
      index++;
    }
  }

  // You'll need to create the image
  vtkSmartPointer<vtkImageData> image = vtkSmartPointer<vtkImageData>::New();

  // Create the image data of size 500, 500, 1, with
  // a ScalarType of VTK_UNSIGNED_CHAR, orgin at (0,0,0),
  // and spacing of (1,1,1).
  image->SetDimensions(500, 500, 1);
  image->SetOrigin(0,0,0);
  image->SetSpacing(1,1,1);

  // Assign the array to the scalars on the
  // point data of the image data
  // todo: give the array to the image data

  vtkSmartPointer<vtkImageThreshold> threshold = vtkSmartPointer<vtkImageThreshold>::New();
  threshold->ThresholdBetween(128,255);
  threshold->SetInputData(image);
  threshold->SetInValue(255);
  threshold->ReplaceInOn();
  threshold->SetOutValue(0.0);
  threshold->ReplaceOutOn();

  // todo: compute the gradient

  // You'll need an image actor for display
  vtkSmartPointer<vtkImageActor> actor = vtkSmartPointer<vtkImageActor>::New();

  // Connect the reader to the image actor
  actor->SetInputData(gradient->GetOutput());

  // Put the image actor in the renderer
  renderer->AddActor(actor);

  // Render and start interactor
  renwin->Render();
  interactor->Start();
}
