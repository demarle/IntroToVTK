from vtk import *

import math

# You'll need a render window to show it in
renwin = vtkRenderWindow()

# You'll need a renderer in the render window
renderer = vtkRenderer()

# Put in renderer in the render window
renwin.AddRenderer(renderer)

# You'll need an interactor for interaction
interactor = vtkRenderWindowInteractor()

# Set interactor on the render window
renwin.SetInteractor(interactor)

style = vtkInteractorStyleImage()
interactor.SetInteractorStyle(style)

# You'll need to create the unsigned char
# array that will hold the data
data = vtkUnsignedCharArray()
data.SetNumberOfComponents(1)
data.SetNumberOfValues(500*500)
index = 0
for i in range(500):
  for j in range(500):
    data.InsertValue(index,(int)(127.5 * (1.0 + math.sin(i/25.0) * math.cos(j/25.0))))
    index += 1

# You'll need to create the image
image = vtkImageData()

# Create the image data of size 500, 500, 1, with
# a ScalarType of VTK_UNSIGNED_CHAR, orgin at (0,0,0),
# and spacing of (1,1,1). 
image.SetDimensions(500, 500, 1)
image.SetOrigin(0,0,0)
image.SetSpacing(1,1,1)

# Assign the array to the scalars on the
# point data of the image data
# todo: give the array to the image data

threshold = vtkImageThreshold()
threshold.ThresholdBetween(128,255);
threshold.SetInputData(image)
threshold.SetInValue(255)
threshold.ReplaceInOn()
threshold.SetOutValue(0.0)
threshold.ReplaceOutOn()

# todo: compute the gradient

# You'll need an image actor for display
actor = vtkImageActor()

# Connect the reader to the image actor
actor.SetInputData(gradient.GetOutput())

# Put the image actor in the renderer
renderer.AddActor(actor)

# Render and start interactor
renwin.Render()
interactor.Start()
