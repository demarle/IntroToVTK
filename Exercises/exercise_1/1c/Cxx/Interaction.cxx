#include <vtkActor.h>
#include <vtkCellData.h>
#include <vtkCellPicker.h>
#include <vtkCommand.h>
#include <vtkPointDataToCellData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRandomAttributeGenerator.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSliderRepresentation3D.h>
#include <vtkSliderWidget.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkWidgetEvent.h>
#include <vtkWidgetEventTranslator.h>

// An observer for slider's events
class vtkSliderCallback : public vtkCommand
{
public:
  static vtkSliderCallback *New()
  {
    return new vtkSliderCallback;
  }
  virtual void Execute(vtkObject *caller, unsigned long, void*)
  {
    // when user drags slider, make a correspondingchange to the visualization
    vtkSliderWidget *sliderWidget =
      reinterpret_cast<vtkSliderWidget*>(caller);

    // todo: change the sphere's resolution
  }
  vtkSliderCallback():SphereSource(0) {}
  vtkSphereSource *SphereSource;
};

// An observer for interactor's 'u' key
class vtkUserCallback : public vtkCommand
{
public:
  static vtkUserCallback *New()
  {
    return new vtkUserCallback;
  }
  virtual void Execute(vtkObject *caller, unsigned long, void*)
  {
    if (this->Renderer)
    {
      // the current pixel
      int *pos = this->Interactor->GetEventPosition();

      // A picker to search behing the current pixel
      vtkSmartPointer<vtkCellPicker> picker =
	vtkSmartPointer<vtkCellPicker>::New();
      picker->SetTolerance(0.0005);
      // do the calculation, similar to Update() or Render()
      picker->Pick(pos[0], pos[1], 0, this->Renderer);

      // translate to a world space position
      double* worldPosition = picker->GetPickPosition();
      cout << "Pick results "
	   << "window coords: "
	   << pos[0] << "," << pos[1] << " "
	   << "world coords: "
	   << worldPosition[0] << ","
	   << worldPosition[1] << ","
	   << worldPosition[2] << endl;

      // check if we hit something
      vtkActor * actor = picker->GetActor();
      if (actor)
      {
	vtkMapper *mapper = actor->GetMapper();
	if (mapper)
	{
	  vtkDataSet *ds = mapper->GetInput();
          // todo: Check if the picker found a valid cell, and if so print something about it
	}
      }
    }
  }
  vtkUserCallback() {}
  vtkWeakPointer<vtkRenderer> Renderer = nullptr;
  vtkWeakPointer<vtkRenderWindowInteractor> Interactor = nullptr;
};

int main (int, char *[])
{
  // A procedurally generated polygonal sphere
  vtkSmartPointer<vtkSphereSource> sphereSource =
    vtkSmartPointer<vtkSphereSource>::New();
  sphereSource->SetCenter(0.0, 0.0, 0.0);
  sphereSource->SetRadius(4.0);
  sphereSource->SetPhiResolution(8);
  sphereSource->SetStartPhi(10.0);
  sphereSource->SetEndPhi(170.0);
  sphereSource->SetThetaResolution(8);

  // Inscribe some values onto the points(vertices)
  vtkSmartPointer<vtkRandomAttributeGenerator> valueMaker =
    vtkSmartPointer<vtkRandomAttributeGenerator>::New();
  valueMaker->SetInputConnection(sphereSource->GetOutputPort());
  valueMaker->SetGeneratePointScalars(1);
  valueMaker->SetGeneratePointVectors(1);

  // Average those onto the cells(triangles)
  vtkSmartPointer<vtkPointDataToCellData> point2cell =
    vtkSmartPointer<vtkPointDataToCellData>::New();
  point2cell->SetInputConnection(valueMaker->GetOutputPort());
  point2cell->SetPassPointData(1);

  // Place the result in the visible scene
  // mapper to make openGL calls
  vtkSmartPointer<vtkPolyDataMapper> mapper =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(point2cell->GetOutputPort());

  // actor to place the object into the scene
  vtkSmartPointer<vtkActor> actor =
    vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);
  actor->GetProperty()->SetInterpolationToFlat();

  // A window on the desktop and a region of pixels within it
  vtkSmartPointer<vtkRenderWindow> renderWindow =
    vtkSmartPointer<vtkRenderWindow>::New();
  vtkSmartPointer<vtkRenderer> renderer =
    vtkSmartPointer<vtkRenderer>::New();
  renderWindow->AddRenderer(renderer);

  // An interactor to catch system and user events
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);

  // Catch the 'u' key and do something with it
  vtkSmartPointer<vtkUserCallback> userCallback =
    vtkSmartPointer<vtkUserCallback>::New();
  userCallback->Renderer = renderer;
  userCallback->Interactor = renderWindowInteractor;
  renderWindowInteractor->AddObserver(vtkCommand::UserEvent,userCallback);

  // Add the actor to the scene
  renderer->AddActor(actor);

  // Make a widget to interact with
  vtkSmartPointer<vtkSliderRepresentation3D> sliderRep =
    vtkSmartPointer<vtkSliderRepresentation3D>::New();
  // modern widgets have independent display ...
  // todo:  note change of what the slider shows and its extent
  sliderRep->SetMinimumValue(0.0);
  sliderRep->SetMaximumValue(360.0);
  sliderRep->SetValue(sphereSource->GetEndTheta());
  sliderRep->SetTitleText("End Theta");
  sliderRep->GetPoint1Coordinate()->SetCoordinateSystemToWorld();
  sliderRep->GetPoint1Coordinate()->SetValue(-4,6,0);
  sliderRep->GetPoint2Coordinate()->SetCoordinateSystemToWorld();
  sliderRep->GetPoint2Coordinate()->SetValue(4,6,0);
  sliderRep->SetSliderLength(0.075);
  sliderRep->SetSliderWidth(0.05);
  sliderRep->SetEndCapLength(0.05);
  // ... and interaction components
  vtkSmartPointer<vtkSliderWidget> sliderWidget =
    vtkSmartPointer<vtkSliderWidget>::New();
  sliderWidget->SetInteractor(renderWindowInteractor);
  sliderWidget->SetRepresentation(sliderRep);
  sliderWidget->SetAnimationModeToAnimate();
  sliderWidget->EnabledOn();

  // Observe the widget's slider events too
  vtkSmartPointer<vtkSliderCallback> sliderCallback =
    vtkSmartPointer<vtkSliderCallback>::New();
  sliderCallback->SphereSource = sphereSource;
  sliderWidget->AddObserver(vtkCommand::InteractionEvent,sliderCallback);

  // Start up the application event loop
  renderWindow->Render();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
