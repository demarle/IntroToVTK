import vtk

SphereSource = None
def vtkSliderCallback(obj, event):
    # An observer for slider's events
    global SphereSource
    # when user drags slider, make a correspondingchange to the visualization
    SphereSource.SetEndTheta(int(obj.GetRepresentation().GetValue()))

Renderer = None
Interactor = None
def UserCallback(obj, event):
    # An observer for interactor's 'u' key
    global Renderer
    global Interactor

    # the current pixel
    pos = Interactor.GetEventPosition()

    # A picker to search behing the current pixel
    picker = vtk.vtkCellPicker()
    picker.SetTolerance(0.0005)
    # do the calculation, similar to Update() or Render()
    picker.Pick(pos[0], pos[1], 0, Renderer)

    # translate to a world space position
    worldPosition = picker.GetPickPosition()
    print("Pick results ",
          "window coords: ",
          pos[0] , "," , pos[1] , " ",
          "world coords: ",
          worldPosition[0] , ",",
          worldPosition[1] , ",",
          worldPosition[2])

    # check if we hit something
    actor = picker.GetActor()
    if actor:
        mapper = actor.GetMapper()
        if mapper:
            ds = mapper.GetInput()
            cellid = picker.GetCellId()
            if (ds and cellid != -1):
                # we got something, ask for the corresponding call aligned value
                value = ds.GetCellData().GetArray("RandomPointScalars").GetTuple1(cellid)
                print("cell " , cellid , "'s value is " , value);

def main():
    # A procedurally generated polygonal sphere
    sphereSource = vtk.vtkSphereSource()
    sphereSource.SetCenter(0.0, 0.0, 0.0)
    sphereSource.SetRadius(4.0)
    sphereSource.SetPhiResolution(8)
    sphereSource.SetStartPhi(10.0)
    sphereSource.SetEndPhi(170.0)
    sphereSource.SetThetaResolution(8)

    # Inscribe some values onto the points(vertices)
    valueMaker = vtk.vtkRandomAttributeGenerator()
    valueMaker.SetInputConnection(sphereSource.GetOutputPort())
    valueMaker.SetGeneratePointScalars(1)
    valueMaker.SetGeneratePointVectors(1)

    # Average those onto the cells(triangles)
    point2cell = vtk.vtkPointDataToCellData()
    point2cell.SetInputConnection(valueMaker.GetOutputPort())
    point2cell.SetPassPointData(1)

    # Place the result in the visible scene
    # mapper to make openGL calls
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputConnection(point2cell.GetOutputPort())

    # actor to place the object into the scene
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetInterpolationToFlat()

    # a window on the desktop and a region of pixels within it
    renderer = vtk.vtkRenderer()
    renderWindow = vtk.vtkRenderWindow()
    renderWindow.AddRenderer(renderer)

    # An interactor to catch system and user events
    renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)

    # Catch the 'u' key and do something with it
    global Renderer
    Renderer = renderer
    global Interactor
    Interactor = renderWindowInteractor
    renderWindowInteractor.AddObserver("UserEvent", UserCallback)

    # Add the actor to the scene
    renderer.AddActor(actor)

    # Make a widget to interact with
    # modern widgets have independent display ...
    sliderRep = vtk.vtkSliderRepresentation3D()
    sliderRep.SetMinimumValue(0)
    sliderRep.SetMaximumValue(360.0)
    sliderRep.SetValue(sphereSource.GetEndTheta())
    sliderRep.SetTitleText("End Theta")
    sliderRep.GetPoint1Coordinate().SetCoordinateSystemToWorld()
    sliderRep.GetPoint1Coordinate().SetValue(-4,6,0)
    sliderRep.GetPoint2Coordinate().SetCoordinateSystemToWorld()
    sliderRep.GetPoint2Coordinate().SetValue(4,6,0)
    sliderRep.SetSliderLength(0.075)
    sliderRep.SetSliderWidth(0.05)
    sliderRep.SetEndCapLength(0.05)
    # ... and interaction components
    sliderWidget = vtk.vtkSliderWidget()
    sliderWidget.SetInteractor(renderWindowInteractor)
    sliderWidget.SetRepresentation(sliderRep)
    sliderWidget.SetAnimationModeToAnimate()
    sliderWidget.EnabledOn()

    # Observe the widget's slider events too
    global SphereSource
    SphereSource = sphereSource
    sliderWidget.AddObserver("InteractionEvent", vtkSliderCallback)

    # Start up the application event loop
    renderWindow.Render()
    renderWindowInteractor.Start()

main()
