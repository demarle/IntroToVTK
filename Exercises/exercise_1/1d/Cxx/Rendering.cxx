#include <vtkActor.h>
#include <vtkCellData.h>
#include <vtkCellPicker.h>
#include <vtkCommand.h>
#include <vtkPointDataToCellData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRandomAttributeGenerator.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSliderRepresentation3D.h>
#include <vtkSliderWidget.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkWidgetEvent.h>
#include <vtkWidgetEventTranslator.h>

// An observer for slider's events
class vtkSliderCallback : public vtkCommand
{
public:
  static vtkSliderCallback *New()
  {
    return new vtkSliderCallback;
  }
  virtual void Execute(vtkObject *caller, unsigned long, void*)
  {
    // when user drags slider, make a correspondingchange to the visualization
    vtkSliderWidget *sliderWidget =
      reinterpret_cast<vtkSliderWidget*>(caller);
    this->SphereSource->SetEndTheta
      (static_cast<vtkSliderRepresentation *>
       (sliderWidget->GetRepresentation())->GetValue());
  }
  vtkSliderCallback():SphereSource(0) {}
  vtkSphereSource *SphereSource;
};

// An observer for interactor's 'u' key
class vtkUserCallback : public vtkCommand
{
public:
  static vtkUserCallback *New()
  {
    return new vtkUserCallback;
  }
  virtual void Execute(vtkObject *caller, unsigned long, void*)
  {
    if (this->Renderer)
    {
      // the current pixel
      int *pos = this->Interactor->GetEventPosition();

      // A picker to search behing the current pixel
      vtkSmartPointer<vtkCellPicker> picker =
	vtkSmartPointer<vtkCellPicker>::New();
      picker->SetTolerance(0.0005);
      // do the calculation, similar to Update() or Render()
      picker->Pick(pos[0], pos[1], 0, this->Renderer);

      // translate to a world space position
      double* worldPosition = picker->GetPickPosition();
      cout << "Pick results "
	   << "window coords: "
	   << pos[0] << "," << pos[1] << " "
	   << "world coords: "
	   << worldPosition[0] << ","
	   << worldPosition[1] << ","
	   << worldPosition[2] << endl;

      // check if we hit something
      vtkActor * actor = picker->GetActor();
      if (actor)
      {
	vtkMapper *mapper = actor->GetMapper();
	if (mapper)
	{
	  vtkDataSet *ds = mapper->GetInput();
	  vtkIdType cellid = picker->GetCellId();
	  if (ds && cellid != -1)
	  {
	    // we got something, ask for the corresponding call aligned value
	    double value = ds->GetCellData()->GetArray("RandomPointScalars")->GetTuple1(cellid);
	    cout << "cell " << cellid << "'s value is " << value << endl;
	  }
	}
      }
    }
  }
  vtkUserCallback() {}
  vtkWeakPointer<vtkRenderer> Renderer = nullptr;
  vtkWeakPointer<vtkRenderWindowInteractor> Interactor = nullptr;
};

int main (int, char *[])
{
  // A procedurally generated polygonal sphere
  vtkSmartPointer<vtkSphereSource> sphereSource =
    vtkSmartPointer<vtkSphereSource>::New();
  sphereSource->SetCenter(0.0, 0.0, 0.0);
  sphereSource->SetRadius(4.0);
  sphereSource->SetPhiResolution(8);
  sphereSource->SetStartPhi(10.0);
  sphereSource->SetEndPhi(170.0);
  sphereSource->SetThetaResolution(8);

  // lazy evaluation we update to get data to populate with
  sphereSource->Update();
  vtkPolyData *sphere = sphereSource->GetOutput();
  double bounds[6];
  sphere->GetBounds(bounds);
  double dx = bounds[1]-bounds[0];
  double cx = bounds[0]+dx/2;
  double dy = bounds[3]-bounds[2];
  double cy = bounds[2]+dy/2;
  double dz = bounds[5]-bounds[4];
  double cz = bounds[4]+dz/2;

  // Inscribe some values onto the points(vertices)
  vtkSmartPointer<vtkRandomAttributeGenerator> valueMaker =
    vtkSmartPointer<vtkRandomAttributeGenerator>::New();
  valueMaker->SetInputConnection(sphereSource->GetOutputPort());
  valueMaker->SetGeneratePointScalars(1);
  valueMaker->SetGeneratePointVectors(1);

  // Average those onto the cells(triangles)
  vtkSmartPointer<vtkPointDataToCellData> point2cell =
    vtkSmartPointer<vtkPointDataToCellData>::New();
  point2cell->SetInputConnection(valueMaker->GetOutputPort());
  point2cell->SetPassPointData(1);

  // Place the result in the visible scene three times drawn different ways

  // version 1 - actor color at left
  vtkSmartPointer<vtkPolyDataMapper> mapper1 =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper1->SetInputConnection(point2cell->GetOutputPort());
  mapper1->ScalarVisibilityOff();
  vtkSmartPointer<vtkActor> actor1 =
    vtkSmartPointer<vtkActor>::New();
  actor1->SetMapper(mapper1);
  actor1->GetProperty()->SetInterpolationToFlat();
  // todo: place actor to the left side
  // todo: set the actor's color

  // version 2 - point aligned data at center
  vtkSmartPointer<vtkPolyDataMapper> mapper2 =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper2->SetInputConnection(point2cell->GetOutputPort());
  vtkSmartPointer<vtkActor> actor2 =
    vtkSmartPointer<vtkActor>::New();
  actor2->SetMapper(mapper2);
  actor2->GetProperty()->SetInterpolationToFlat();
  actor2->SetPosition(cx, cy, cz);

  // version 3 - cell aligned data at right
  vtkSmartPointer<vtkPolyDataMapper> mapper3 =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper3->SetInputConnection(point2cell->GetOutputPort());
  // todo: change the mapper to color by a cell array 
  vtkSmartPointer<vtkActor> actor3 =
    vtkSmartPointer<vtkActor>::New();
  actor3->SetMapper(mapper3);
  actor3->GetProperty()->SetInterpolationToFlat();
  // todo: place actor to the right side 

  // A window on the desktop and a region of pixels within it
  vtkSmartPointer<vtkRenderWindow> renderWindow =
    vtkSmartPointer<vtkRenderWindow>::New();
  vtkSmartPointer<vtkRenderer> renderer =
    vtkSmartPointer<vtkRenderer>::New();
  renderWindow->AddRenderer(renderer);
  // todo: turn on gradient color and sete top and bottom colors
  //renderer->GetActiveCamera()->ParallelProjectionOn();

  // An interactor to catch system and user events
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);

  // Catch the 'u' key and do something with it
  vtkSmartPointer<vtkUserCallback> userCallback =
    vtkSmartPointer<vtkUserCallback>::New();
  userCallback->Renderer = renderer;
  userCallback->Interactor = renderWindowInteractor;
  renderWindowInteractor->AddObserver(vtkCommand::UserEvent,userCallback);

  // todo: Add the actors to the scene

  // Make a widget to interact with
  vtkSmartPointer<vtkSliderRepresentation3D> sliderRep =
    vtkSmartPointer<vtkSliderRepresentation3D>::New();
  // modern widgets have independent display ...
  sliderRep->SetMinimumValue(0.0);
  sliderRep->SetMaximumValue(360.0);
  sliderRep->SetValue(sphereSource->GetEndTheta());
  sliderRep->SetTitleText("End Theta");
  sliderRep->GetPoint1Coordinate()->SetCoordinateSystemToWorld();
  sliderRep->GetPoint1Coordinate()->SetValue(-4,6,0);
  sliderRep->GetPoint2Coordinate()->SetCoordinateSystemToWorld();
  sliderRep->GetPoint2Coordinate()->SetValue(4,6,0);
  sliderRep->SetSliderLength(0.075);
  sliderRep->SetSliderWidth(0.05);
  sliderRep->SetEndCapLength(0.05);
  // ... and interaction components
  vtkSmartPointer<vtkSliderWidget> sliderWidget =
    vtkSmartPointer<vtkSliderWidget>::New();
  sliderWidget->SetInteractor(renderWindowInteractor);
  sliderWidget->SetRepresentation(sliderRep);
  sliderWidget->SetAnimationModeToAnimate();
  sliderWidget->EnabledOn();

  // Observe the widget's slider events too
  vtkSmartPointer<vtkSliderCallback> sliderCallback =
    vtkSmartPointer<vtkSliderCallback>::New();
  sliderCallback->SphereSource = sphereSource;
  sliderWidget->AddObserver(vtkCommand::InteractionEvent,sliderCallback);

  // Start up the application event loop
  renderWindow->Render();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
