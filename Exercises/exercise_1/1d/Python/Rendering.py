import vtk

SphereSource = None
def vtkSliderCallback(obj, event):
    # An observer for slider's events
    global SphereSource
    # when user drags slider, make a correspondingchange to the visualization
    SphereSource.SetEndTheta(int(obj.GetRepresentation().GetValue()))

Renderer = None
Interactor = None
def UserCallback(obj, event):
    # An observer for interactor's 'u' key
    global Renderer
    global Interactor

    # the current pixel
    pos = Interactor.GetEventPosition()

    # A picker to search behing the current pixel
    picker = vtk.vtkCellPicker()
    picker.SetTolerance(0.0005)
    # do the calculation, similar to Update() or Render()
    picker.Pick(pos[0], pos[1], 0, Renderer)

    # translate to a world space position
    worldPosition = picker.GetPickPosition()
    print("Pick results ",
	  "window coords: ",
	  pos[0] , "," , pos[1] , " ",
	  "world coords: ",
	  worldPosition[0] , ",",
	  worldPosition[1] , ",",
	  worldPosition[2])

    # check if we hit something
    actor = picker.GetActor()
    if actor:
	mapper = actor.GetMapper()
	if mapper:
	    ds = mapper.GetInput()
	    cellid = picker.GetCellId()
	    if (ds and cellid != -1):
		# we got something, ask for the corresponding call aligned value
		value = ds.GetCellData().GetArray("RandomPointScalars").GetTuple1(cellid)
		print("cell " , cellid , "'s value is " , value);

def main():
    # A procedurally generated polygonal sphere
    sphereSource = vtk.vtkSphereSource()
    sphereSource.SetCenter(0.0, 0.0, 0.0)
    sphereSource.SetRadius(4.0)
    sphereSource.SetPhiResolution(8)
    sphereSource.SetStartPhi(10.0)
    sphereSource.SetEndPhi(170.0)
    sphereSource.SetThetaResolution(8)

    # lazy evaluation we update to get data to populate with
    sphereSource.Update()
    sphere = sphereSource.GetOutput()
    bounds = sphere.GetBounds()
    dx = bounds[1]-bounds[0]
    cx = bounds[0]+dx/2
    dy = bounds[3]-bounds[2]
    cy = bounds[2]+dy/2
    dz = bounds[5]-bounds[4]
    cz = bounds[4]+dz/2

    # Inscribe some values onto the points(vertices)
    valueMaker = vtk.vtkRandomAttributeGenerator()
    valueMaker.SetInputConnection(sphereSource.GetOutputPort())
    valueMaker.SetGeneratePointScalars(1)
    valueMaker.SetGeneratePointVectors(1)

    # Average those onto the cells(triangles)
    point2cell = vtk.vtkPointDataToCellData()
    point2cell.SetInputConnection(valueMaker.GetOutputPort())
    point2cell.SetPassPointData(1)

    # Place the result in the visible scene three times drawn different ways
    # version 1 - actor color at left
    mapper1 = vtk.vtkPolyDataMapper()
    mapper1.SetInputConnection(point2cell.GetOutputPort())
    mapper1.ScalarVisibilityOff()
    actor1 = vtk.vtkActor()
    actor1.SetMapper(mapper1)
    actor1.GetProperty().SetInterpolationToFlat()
    #todo: place actor to the left side
    #todo: set the actor's color

    # version 2 - point aligned data at center
    mapper2 = vtk.vtkPolyDataMapper()
    mapper2.SetInputConnection(point2cell.GetOutputPort())
    actor2 = vtk.vtkActor()
    actor2.SetMapper(mapper2)
    actor2.GetProperty().SetInterpolationToFlat()
    actor2.SetPosition(cx, cy, cz)

    # version 3 - cell aligned data at right
    mapper3 = vtk.vtkPolyDataMapper()
    mapper3.SetInputConnection(point2cell.GetOutputPort())
    # todo: change the mapper to color by a cell array 
    actor3 = vtk.vtkActor()
    actor3.SetMapper(mapper3)
    actor3.GetProperty().SetInterpolationToFlat()
    # todo: place actor to the right side

    # A window on the desktop and a region of pixels within it
    renderer = vtk.vtkRenderer()
    renderWindow = vtk.vtkRenderWindow()
    renderWindow.AddRenderer(renderer)
    # todo: turn on gradient color and sete top and bottom colors
    #renderer.GetActiveCamera().ParallelProjectionOn()

    # An interactor to catch system and user events
    renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)

    # Catch the 'u' key and do something with it
    global Renderer
    Renderer = renderer
    global Interactor
    Interactor = renderWindowInteractor
    renderWindowInteractor.AddObserver("UserEvent", UserCallback)

    # todo: Add the actors to the scene

    # Make a widget to interact with
    # modern widgets have independent display ...
    sliderRep = vtk.vtkSliderRepresentation3D()
    sliderRep.SetMinimumValue(0.0)
    sliderRep.SetMaximumValue(360.0)
    sliderRep.SetValue(sphereSource.GetThetaResolution())
    sliderRep.SetTitleText("Sphere Resolution")
    sliderRep.GetPoint1Coordinate().SetCoordinateSystemToWorld()
    sliderRep.GetPoint1Coordinate().SetValue(-4,6,0)
    sliderRep.GetPoint2Coordinate().SetCoordinateSystemToWorld()
    sliderRep.GetPoint2Coordinate().SetValue(4,6,0)
    sliderRep.SetSliderLength(0.075)
    sliderRep.SetSliderWidth(0.05)
    sliderRep.SetEndCapLength(0.05)
    # ... and interaction components
    sliderWidget = vtk.vtkSliderWidget()
    sliderWidget.SetInteractor(renderWindowInteractor)
    sliderWidget.SetRepresentation(sliderRep)
    sliderWidget.SetAnimationModeToAnimate()
    sliderWidget.EnabledOn()

    # Observe the widget's slider events too
    global SphereSource
    SphereSource = sphereSource
    sliderWidget.AddObserver("InteractionEvent", vtkSliderCallback)

    # Start up the application event loop
    renderWindow.Render()
    renderWindowInteractor.Start()

main()
