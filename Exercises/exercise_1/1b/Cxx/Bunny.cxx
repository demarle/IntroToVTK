#include <vtkActor.h>
#include <vtkCommand.h>
#include <vtkDataSetWriter.h>
#include <vtkPLYReader.h>
#include <vtkPointDataToCellData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRandomAttributeGenerator.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkWeakPointer.h>

// An observer for interactor's 'u' key
class vtkUserCallback : public vtkCommand
{
public:
  static vtkUserCallback *New()
  {
    return new vtkUserCallback;
  }
  virtual void Execute(vtkObject *caller, unsigned long, void*)
  {
    if (this->Filter)
    {
      cout << "Writing to file.vtk" << endl;
      vtkSmartPointer<vtkDataSetWriter> writer =
	vtkSmartPointer<vtkDataSetWriter>::New();
      writer->SetInputConnection(this->Filter->GetOutputPort());
      writer->SetFileName("file.vtk");
      writer->Write();

      //// demonstrate printing to console as well
      //this->Filter->GetOutputDataObject(0)->PrintSelf(cout, vtkIndent(0));
    }
  }
  vtkUserCallback() {}
  vtkWeakPointer<vtkAlgorithm> Filter = nullptr;
};

int main (int, char *[])
{
  // todo: make a reader for ply files instead of the sphere source

  // todo: get the num points and cells and bounds of the reader's output

  // Inscribe some values onto the points(vertices)
  vtkSmartPointer<vtkRandomAttributeGenerator> valueMaker =
    vtkSmartPointer<vtkRandomAttributeGenerator>::New();
  valueMaker->SetInputConnection(plyReader->GetOutputPort());
  valueMaker->SetGeneratePointScalars(1);
  valueMaker->SetGeneratePointVectors(1);

  // Average those onto the cells(triangles)
  vtkSmartPointer<vtkPointDataToCellData> point2cell =
    vtkSmartPointer<vtkPointDataToCellData>::New();
  point2cell->SetInputConnection(valueMaker->GetOutputPort());
  point2cell->SetPassPointData(1);

  // Place the result in the visible scene
  // mapper to make openGL calls
  vtkSmartPointer<vtkPolyDataMapper> mapper =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(point2cell->GetOutputPort());

  // actor to place the object into the scene
  vtkSmartPointer<vtkActor> actor =
    vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);
  actor->GetProperty()->SetInterpolationToFlat();

  // A window on the desktop and a region of pixels within it
  vtkSmartPointer<vtkRenderWindow> renderWindow =
    vtkSmartPointer<vtkRenderWindow>::New();
  vtkSmartPointer<vtkRenderer> renderer =
    vtkSmartPointer<vtkRenderer>::New();
  renderWindow->AddRenderer(renderer);

  // An interactor to catch system and user events
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);

  // Observe the 'u' key to do something with it
  vtkSmartPointer<vtkUserCallback> userCallback =
    vtkSmartPointer<vtkUserCallback>::New();
  userCallback->Filter = point2cell;
  renderWindowInteractor->AddObserver(vtkCommand::UserEvent,userCallback);

  // Add the actor to the scene
  renderer->AddActor(actor);

  // Start up the application event loop
  renderWindow->Render();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
