import vtk

PointToCell = None
def UserCallback(obj, event):
    # An observer for slider's events
    global PointToCell
    print("Writing to file.vtk")
    writer = vtk.vtkDataSetWriter()
    writer.SetInputConnection(PointToCell.GetOutputPort())
    writer.SetFileName("file.vtk")
    writer.Write()

    ## demonstrate printing to console as well
    #print(PointToCell.GetOutputDataObject(0))

def main():
    # todo: make a reader for ply files instead of the sphere source

    # todo: get the num points and cells and bounds of the reader's output

    # Inscribe some values onto the points(vertices)
    valueMaker = vtk.vtkRandomAttributeGenerator()
    valueMaker.SetInputConnection(plyReader.GetOutputPort())
    valueMaker.SetGeneratePointScalars(1)
    valueMaker.SetGeneratePointVectors(1)

    # Average those onto the cells(triangles)
    point2cell = vtk.vtkPointDataToCellData()
    point2cell.SetInputConnection(valueMaker.GetOutputPort())
    point2cell.SetPassPointData(1)

    # Place the result in the visible scene
    # mapper to make openGL calls
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputConnection(point2cell.GetOutputPort())

    # actor to place the object into the scene
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetInterpolationToFlat()

    # A window on the desktop and a region of pixels within it
    renderer = vtk.vtkRenderer()
    renderWindow = vtk.vtkRenderWindow()
    renderWindow.AddRenderer(renderer)

    # An interactor to catch system and user events
    renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)

    # Observe the 'u' key to do something with it
    global PointToCell
    PointToCell = point2cell
    renderWindowInteractor.AddObserver("UserEvent", UserCallback)

    # Add the actor to the scene
    renderer.AddActor(actor)

    # Start up the application event loop
    renderWindowInteractor.Initialize()
    renderWindow.Render()

    renderWindowInteractor.Start()

main()
