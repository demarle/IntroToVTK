#include <vtkActor.h>
#include <vtkCommand.h>
#include <vtkContourFilter.h>
#include <vtkDataSetMapper.h>
#include <vtkDataSetWriter.h>
#include <vtkLookupTable.h>
#include <vtkPointDataToCellData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRandomAttributeGenerator.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkScalarBarActor.h>
#include <vtkSliderRepresentation3D.h>
#include <vtkSliderWidget.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>

// An observer for slider's events
class vtkSliderCallback : public vtkCommand
{
public:
  static vtkSliderCallback *New()
  {
    return new vtkSliderCallback;
  }
  virtual void Execute(vtkObject *caller, unsigned long, void*)
  {
    // when user drags slider, make a corresponding change to the visualization
    vtkSliderWidget *sliderWidget =
      reinterpret_cast<vtkSliderWidget*>(caller);
    this->ContourFilter->SetValue(0,static_cast<vtkSliderRepresentation *>(sliderWidget->GetRepresentation())->GetValue());
  }
  vtkSliderCallback():ContourFilter(nullptr) {}
  vtkContourFilter *ContourFilter;
};

// An observer for interactor's 'u' key
class vtkUserCallback : public vtkCommand
{
public:
  static vtkUserCallback *New()
  {
    return new vtkUserCallback;
  }
  virtual void Execute(vtkObject *caller, unsigned long, void*)
  {
    // An observer for interactor's 'u' key
    if (this->Filter)
    {
      cout << "Writing to file.vtk" << endl;
      vtkSmartPointer<vtkDataSetWriter> writer =
	vtkSmartPointer<vtkDataSetWriter>::New();
      writer->SetInputConnection(this->Filter->GetOutputPort());
      writer->SetFileName("file.vtk");
      writer->Write();

      // demonstrate printing to console as well
      this->Filter->GetOutputDataObject(0)->PrintSelf(cout, vtkIndent(0));
    }
  }
  vtkUserCallback() {}
  vtkWeakPointer<vtkAlgorithm> Filter = nullptr;
};

int main (int, char *[])
{
  // A procedurally generated polygonalsphere
  vtkSmartPointer<vtkSphereSource> sphereSource =
    vtkSmartPointer<vtkSphereSource>::New();
  sphereSource->SetCenter(0.0, 0.0, 0.0);
  sphereSource->SetRadius(4.0);
  sphereSource->SetPhiResolution(8);
  sphereSource->SetStartPhi(10.0);
  sphereSource->SetEndPhi(170.0);
  sphereSource->SetThetaResolution(8);

  // Inscribe some values onto the points(vertices)
  vtkSmartPointer<vtkRandomAttributeGenerator> valueMaker =
    vtkSmartPointer<vtkRandomAttributeGenerator>::New();
  valueMaker->SetInputConnection(sphereSource->GetOutputPort());
  valueMaker->SetGeneratePointScalars(1);
  valueMaker->SetGeneratePointVectors(1);

  // Average those onto the cells(triangles)
  vtkSmartPointer<vtkPointDataToCellData> point2cell =
    vtkSmartPointer<vtkPointDataToCellData>::New();
  point2cell->SetInputConnection(valueMaker->GetOutputPort());
  point2cell->SetPassPointData(1);

  // Place that into the visible scene
  // mapper to make openGL calls
  vtkSmartPointer<vtkPolyDataMapper> mapper =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(point2cell->GetOutputPort());
  //interpolate value first then compute color or compute color first and interpolate that
  mapper->SetInterpolateScalarsBeforeMapping(1);

  // Programmatically define a lookuptable
  vtkSmartPointer<vtkLookupTable> lut =
    vtkSmartPointer<vtkLookupTable>::New();
  lut->SetTableRange(0.0, 1.0);
  int divs = 100;
  lut->SetNumberOfTableValues(divs);
  // a smooth red to white to blue colormap
  int half = divs/2;
  for (int i=0; i < divs; i++)
  {
    if (i < half)
    {
      // todo: red and green from 0..1
    }
    else
    {
      int local=i-half;
      // todo: green and blue from 1 to 0
    }
  }
  mapper->SetLookupTable(lut);
  lut->SetAnnotation(0.1, "0.1");
  lut->SetAnnotation(0.5, "0.5");
  lut->SetAnnotation(0.9, "0.9");

  // actor to place the object into the scene
  vtkSmartPointer<vtkActor> actor =
    vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);
  actor->GetProperty()->SetInterpolationToFlat();
  actor->GetProperty()->SetColor(0,0,1);

  // A window on the desktop and a region of pixels within it
  vtkSmartPointer<vtkRenderWindow> renderWindow =
    vtkSmartPointer<vtkRenderWindow>::New();
  vtkSmartPointer<vtkRenderer> renderer =
    vtkSmartPointer<vtkRenderer>::New();
  renderWindow->AddRenderer(renderer);

  // An interactor to catch system and user events
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);

  // Catch the 'u' key and do something with it
  vtkSmartPointer<vtkUserCallback> userCallback =
    vtkSmartPointer<vtkUserCallback>::New();
  userCallback->Filter = point2cell;
  renderWindowInteractor->AddObserver(vtkCommand::UserEvent,userCallback);

  // make a contour filter to highlight a specific value
  vtkSmartPointer<vtkContourFilter> contour =
    vtkSmartPointer<vtkContourFilter>::New();
  contour->SetInputConnection(point2cell->GetOutputPort());
  contour->SetNumberOfContours(1);
  contour->SetValue(0, 0.5);

  // Make a widget to interact with
  // modern widgets have independent display ...
  vtkSmartPointer<vtkSliderRepresentation3D> sliderRep =
    vtkSmartPointer<vtkSliderRepresentation3D>::New();
  sliderRep->SetMinimumValue(0);
  sliderRep->SetMaximumValue(1.0);
  sliderRep->SetValue(0.5);
  sliderRep->SetTitleText("");
  sliderRep->GetPoint1Coordinate()->SetCoordinateSystemToWorld();
  sliderRep->GetPoint1Coordinate()->SetValue(-4,6,0);
  sliderRep->GetPoint2Coordinate()->SetCoordinateSystemToWorld();
  sliderRep->GetPoint2Coordinate()->SetValue(4,6,0);
  sliderRep->SetSliderLength(0.075);
  sliderRep->SetSliderWidth(0.05);
  sliderRep->SetEndCapLength(0.05);
  // ... and interaction components
  vtkSmartPointer<vtkSliderWidget> sliderWidget =
    vtkSmartPointer<vtkSliderWidget>::New();
  sliderWidget->SetInteractor(renderWindowInteractor);
  sliderWidget->SetRepresentation(sliderRep);
  sliderWidget->SetAnimationModeToAnimate();
  sliderWidget->EnabledOn();

  // Observe the widget's slider events too
  vtkSmartPointer<vtkSliderCallback> sliderCallback =
    vtkSmartPointer<vtkSliderCallback>::New();
  sliderWidget->AddObserver(vtkCommand::InteractionEvent,sliderCallback);
  sliderCallback->ContourFilter = contour;

  // make a scalar bar to show value to color mapping
  vtkSmartPointer<vtkScalarBarActor> scalarBar =
    vtkSmartPointer<vtkScalarBarActor>::New();
  scalarBar->SetLookupTable(mapper->GetLookupTable());
  scalarBar->SetTitle("Pt Scalars");
  scalarBar->SetNumberOfLabels(3);
  // a mapper for the scalar bar
  vtkSmartPointer<vtkDataSetMapper> cmap =
    vtkSmartPointer<vtkDataSetMapper>::New();
  cmap->SetInputConnection(contour->GetOutputPort());
  cmap->ScalarVisibilityOff();
  // and an actor
  vtkSmartPointer<vtkActor> cactor =
    vtkSmartPointer<vtkActor>::New();
  cactor->GetProperty()->SetLineWidth(5);
  cactor->GetProperty()->SetColor(1,1,0);
  cactor->SetMapper(cmap);

  // todo: Add everything to the scene

  // Start up the application event loop
  renderWindow->Render();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
