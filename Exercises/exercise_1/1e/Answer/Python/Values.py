import vtk

ContourFilter = None
def vtkSliderCallback(obj, event):
    # An observer for slider's events
    global ContourFilter
    # when user drags slider, make a correspondingchange to the visualization
    ContourFilter.SetValue(0, obj.GetRepresentation().GetValue())

PointToCell = None
def UserCallback(obj, event):
    # An observer for interactor's 'u' key
    global PointToCell
    print("Writing to file.vtk")
    writer = vtk.vtkDataSetWriter()
    writer.SetInputConnection(PointToCell.GetOutputPort())
    writer.SetFileName("file.vtk")
    writer.Write()
    # demonstrate printing to console as well
    print(PointToCell.GetOutputDataObject(0))

def main():
    # A procedurally generated polygonal sphere
    sphereSource = vtk.vtkSphereSource()
    sphereSource.SetCenter(0.0, 0.0, 0.0)
    sphereSource.SetRadius(4.0)
    sphereSource.SetPhiResolution(8)
    sphereSource.SetStartPhi(10.0)
    sphereSource.SetEndPhi(170.0)
    sphereSource.SetThetaResolution(8)
    sphereSource.Update()

    # Inscribe some values onto the points(vertices)
    valueMaker = vtk.vtkRandomAttributeGenerator()
    valueMaker.SetInputConnection(sphereSource.GetOutputPort())
    valueMaker.SetGeneratePointScalars(1)
    valueMaker.SetGeneratePointVectors(1)

    # Average those onto the cells(triangles)
    point2cell = vtk.vtkPointDataToCellData()
    point2cell.SetInputConnection(valueMaker.GetOutputPort())
    point2cell.SetPassPointData(1)

    # Place the result in the visible scene
    # mapper to make openGL calls
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputConnection(point2cell.GetOutputPort())
    # interpolate value first then compute color or compute color first and interpolate that
    mapper.SetInterpolateScalarsBeforeMapping(1)

    # Programmatically define a lookuptable
    lut = vtk.vtkLookupTable()
    lut.SetTableRange(0.0, 1.0)
    divs = 100
    lut.SetNumberOfTableValues(divs)
    # a smooth red to white to blue colormap
    half = divs/2.0
    for i in range(0, divs):
        if i < half:
            lut.SetTableValue(i,  i/half, i/half, 1.0)
        else:
           local=i-half
           lut.SetTableValue(i,  1.0, (half-local)/half, (half-local)/half)
    mapper.SetLookupTable(lut)
    lut.SetAnnotation(0.1, "0.1")
    lut.SetAnnotation(0.5, "0.5")
    lut.SetAnnotation(0.9, "0.9")

    # actor to place the object into the scene
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetInterpolationToFlat()
    actor.GetProperty().SetColor(0,0,1)

    # a window on the desktop and a region of pixels within it
    renderer = vtk.vtkRenderer()
    renderWindow = vtk.vtkRenderWindow()
    renderWindow.AddRenderer(renderer)

    # An interactor to catch system and user events
    renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)

    # Catch the 'u' key and do something with it
    global PointToCell
    PointToCell = point2cell
    renderWindowInteractor.AddObserver("UserEvent", UserCallback)

    # make a contour filter to highlight a specific value
    contour = vtk.vtkContourFilter()
    contour.SetInputConnection(point2cell.GetOutputPort())
    contour.SetNumberOfContours(1)
    contour.SetValue(0, 0.5)

    # Make a widget to interact with
    # modern widgets have independent display ...
    sliderRep = vtk.vtkSliderRepresentation3D()
    sliderRep.SetMinimumValue(0)
    sliderRep.SetMaximumValue(1.0)
    sliderRep.SetValue(0.5)
    sliderRep.SetTitleText("")
    sliderRep.GetPoint1Coordinate().SetCoordinateSystemToWorld()
    sliderRep.GetPoint1Coordinate().SetValue(-4,6,0)
    sliderRep.GetPoint2Coordinate().SetCoordinateSystemToWorld()
    sliderRep.GetPoint2Coordinate().SetValue(4,6,0)
    sliderRep.SetSliderLength(0.075)
    sliderRep.SetSliderWidth(0.05)
    sliderRep.SetEndCapLength(0.05)
    # ... and interaction components
    sliderWidget = vtk.vtkSliderWidget()
    sliderWidget.SetInteractor(renderWindowInteractor)
    sliderWidget.SetRepresentation(sliderRep)
    sliderWidget.SetAnimationModeToAnimate()
    sliderWidget.EnabledOn()

    # Observe the widget's slider events too
    global ContourFilter
    ContourFilter = contour
    sliderWidget.AddObserver("InteractionEvent", vtkSliderCallback)

    # make a scalar bar to show value to color mapping
    scalarBar = vtk.vtkScalarBarActor()
    scalarBar.SetLookupTable(mapper.GetLookupTable())
    scalarBar.SetTitle("Pt Scalars")
    scalarBar.SetNumberOfLabels(3)
    # a mapper for the scalar bar
    cmap = vtk.vtkDataSetMapper()
    cmap.SetInputConnection(contour.GetOutputPort())
    cmap.ScalarVisibilityOff()
    # and an actor
    cactor = vtk.vtkActor()
    cactor.GetProperty().SetLineWidth(5.0)
    cactor.GetProperty().SetColor(1,1,0)
    cactor.SetMapper(cmap)

    # Add everything to the scene actor to the scene
    renderer.AddActor(actor)
    renderer.AddActor(cactor)
    renderer.AddActor2D(scalarBar)

    # Start up the application event loop
    renderWindow.Render()
    renderWindowInteractor.Start()

main()
