#include <vtkActor.h>
#include <vtkCellData.h>
#include <vtkCellTypes.h>
#include <vtkCommand.h>
#include <vtkDataSetWriter.h>
#include <vtkPointData.h>
#include <vtkPointDataToCellData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRandomAttributeGenerator.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSliderRepresentation3D.h>
#include <vtkSliderWidget.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>

// An observer for slider's events
class vtkSliderCallback : public vtkCommand
{
public:
  static vtkSliderCallback *New()
  {
    return new vtkSliderCallback;
  }
  virtual void Execute(vtkObject *caller, unsigned long, void*)
  {
    // when user drags slider, make a correspondingchange to the visualization
    vtkSliderWidget *sliderWidget =
      reinterpret_cast<vtkSliderWidget*>(caller);
    this->SphereSource->SetPhiResolution
      (static_cast<vtkSliderRepresentation *>
       (sliderWidget->GetRepresentation())->GetValue()/2);
    this->SphereSource->SetThetaResolution
      (static_cast<vtkSliderRepresentation *>
       (sliderWidget->GetRepresentation())->GetValue());
  }
  vtkSliderCallback():SphereSource(0) {}
  vtkSphereSource *SphereSource;
};

// An observer for interactor's 'u' key
class vtkUserCallback : public vtkCommand
{
public:
  static vtkUserCallback *New()
  {
    return new vtkUserCallback;
  }
  virtual void Execute(vtkObject *caller, unsigned long, void*)
  {
    if (this->Filter)
    {
      // a writer to save the data to disk
      cout << "Writing to file.vtk" << endl;
      vtkSmartPointer<vtkDataSetWriter> writer =
	vtkSmartPointer<vtkDataSetWriter>::New();
      writer->SetInputConnection(this->Filter->GetOutputPort());
      writer->SetFileName("file.vtk");
      writer->Write();

      //// demonstrate printing to console as well
      //this->Filter->GetOutputData()->PrintSelf(cout, vtkIndent(0));

      // or do something like it by hand
      vtkDataSet *ds = this->Filter->GetOutput();
      cerr << ds->GetClassName() << endl;
      vtkIdType ncells = ds->GetNumberOfCells();
      cerr << ncells << " cells" << endl;
      vtkCell *c = ds->GetCell(ncells-1);
      cerr << "Last Cell is a " << vtkCellTypes::GetClassNameFromTypeId(c->GetCellType()) << endl;
      vtkIdType npoints = ds->GetNumberOfPoints();
      cerr << npoints << " points" << endl;
      double *p = ds->GetPoint(npoints-1);
      cerr << "Last Point is at " << p[0] << "," << p[1] << "," << p[2] << endl;

      vtkIdType narrays = ds->GetCellData()->GetNumberOfArrays();
      cerr << narrays << " cell aligned arrays" << endl;
      for (int i = 0; i < narrays; i++)
      {
	vtkDataArray *da = ds->GetCellData()->GetArray(0);
	cerr << da->GetName() << " " << da->GetClassName() << endl;
	vtkIdType ncomponents = da->GetNumberOfComponents();
	for (int j = 0; j < ncomponents; j++)
	{
	  double *r = da->GetRange(j);
	  cerr << "C" << j << ": range is " << r[0] << " to " << r[1] << endl;
	  cerr << "C" << j << " last entry is " << da->GetComponent(ncells-1, j) << endl;
	}
      }
      narrays = ds->GetPointData()->GetNumberOfArrays();
      cerr << narrays << " point aligned arrays" << endl;
      for (int i = 0; i < narrays; i++)
      {
	vtkDataArray *da = ds->GetPointData()->GetArray(0);
	cerr << da->GetName() << " " << da->GetClassName() << endl;
	vtkIdType ncomponents = da->GetNumberOfComponents();
	for (int j = 0; j < ncomponents; j++)
	{
	  double *r = da->GetRange(j);
	  cerr << "C" << j << ": range is " << r[0] << " to " << r[1] << endl;
	  cerr << "C" << j << " last entry is " << da->GetComponent(npoints-1, j) << endl;
	}
      }

    }
  }
  vtkUserCallback() {}
  vtkWeakPointer<vtkDataSetAlgorithm> Filter = nullptr;
};

int main (int, char *[])
{
  // A procedurally generated polygonal sphere
  vtkSmartPointer<vtkSphereSource> sphereSource =
    vtkSmartPointer<vtkSphereSource>::New();
  sphereSource->SetCenter(0.0, 0.0, 0.0);
  sphereSource->SetRadius(4.0);
  sphereSource->SetPhiResolution(8);
  sphereSource->SetStartPhi(10.0);
  sphereSource->SetEndPhi(170.0);
  sphereSource->SetThetaResolution(8);

  // Inscribe some values onto the points(vertices)
  vtkSmartPointer<vtkRandomAttributeGenerator> valueMaker =
    vtkSmartPointer<vtkRandomAttributeGenerator>::New();
  valueMaker->SetInputConnection(sphereSource->GetOutputPort());
  valueMaker->SetGeneratePointScalars(1);
  valueMaker->SetGeneratePointVectors(1);

  // Average those onto the cells(triangles)
  vtkSmartPointer<vtkPointDataToCellData> point2cell =
    vtkSmartPointer<vtkPointDataToCellData>::New();
  point2cell->SetInputConnection(valueMaker->GetOutputPort());
  point2cell->SetPassPointData(1);

  // Place the result in the visible scene
  // mapper to make openGL calls
  vtkSmartPointer<vtkPolyDataMapper> mapper =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(point2cell->GetOutputPort());

  // actor to place the object into the scene
  vtkSmartPointer<vtkActor> actor =
    vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);
  actor->GetProperty()->SetInterpolationToFlat();

  // A window on the desktop and a region of pixels within it
  vtkSmartPointer<vtkRenderWindow> renderWindow =
    vtkSmartPointer<vtkRenderWindow>::New();
  vtkSmartPointer<vtkRenderer> renderer =
    vtkSmartPointer<vtkRenderer>::New();
  renderWindow->AddRenderer(renderer);

  // An interactor to catch system and user events
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);

  // Observe the 'u' key to do something with it
  vtkSmartPointer<vtkUserCallback> userCallback =
    vtkSmartPointer<vtkUserCallback>::New();
  userCallback->Filter = point2cell;
  renderWindowInteractor->AddObserver(vtkCommand::UserEvent,userCallback);

  // Add the actor to the scene
  renderer->AddActor(actor);

  // Make a widget to interact with
  // modern widgets have independent display ...
  vtkSmartPointer<vtkSliderRepresentation3D> sliderRep =
    vtkSmartPointer<vtkSliderRepresentation3D>::New();
  sliderRep->SetMinimumValue(3.0);
  sliderRep->SetMaximumValue(50.0);
  sliderRep->SetValue(sphereSource->GetThetaResolution());
  sliderRep->SetTitleText("Sphere Resolution");
  sliderRep->GetPoint1Coordinate()->SetCoordinateSystemToWorld();
  sliderRep->GetPoint1Coordinate()->SetValue(-4,6,0);
  sliderRep->GetPoint2Coordinate()->SetCoordinateSystemToWorld();
  sliderRep->GetPoint2Coordinate()->SetValue(4,6,0);
  sliderRep->SetSliderLength(0.075);
  sliderRep->SetSliderWidth(0.05);
  sliderRep->SetEndCapLength(0.05);
  // ... and interaction components
  vtkSmartPointer<vtkSliderWidget> sliderWidget =
    vtkSmartPointer<vtkSliderWidget>::New();
  sliderWidget->SetInteractor(renderWindowInteractor);
  sliderWidget->SetRepresentation(sliderRep);
  sliderWidget->SetAnimationModeToAnimate();
  sliderWidget->EnabledOn();

  // Observe the widget's slider events too
  vtkSmartPointer<vtkSliderCallback> sliderCallback =
    vtkSmartPointer<vtkSliderCallback>::New();
  sliderCallback->SphereSource = sphereSource;
  sliderWidget->AddObserver(vtkCommand::InteractionEvent,sliderCallback);

  // Start up the application event loop
  renderWindow->Render();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
