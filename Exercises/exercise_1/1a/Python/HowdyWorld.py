import vtk

SphereSource = None
def vtkSliderCallback(obj, event):
    # An observer for slider's events
    global SphereSource
    # when user drags slider, make a correspondingchange to the visualization
    SphereSource.SetPhiResolution(int(obj.GetRepresentation().GetValue()/2))
    SphereSource.SetThetaResolution(int(obj.GetRepresentation().GetValue()/2))

PointToCell = None
def UserCallback(obj, event):
    # An observer for interactor's 'u' key
    global PointToCell
    print("Writing to file.vtk")
    writer = vtk.vtkDataSetWriter()
    writer.SetInputConnection(PointToCell.GetOutputPort())
    writer.SetFileName("file.vtk")
    writer.Write()

    ## demonstrate printing to console as well
    #print(PointToCell.GetOutput(0))

    # or do something like it by hand

    ds = PointToCell.GetOutput()
    # todo:
    # print dataset type
    # print number of cells
    # print type of last cell
    # print number of points
    # print coordinates of last points
    # print cell arrays and ranges
    # print point arrays and ranges

def main():
    # A procedurally generated polygonal sphere
    sphereSource = vtk.vtkSphereSource()
    sphereSource.SetCenter(0.0, 0.0, 0.0)
    sphereSource.SetRadius(4.0)
    sphereSource.SetPhiResolution(8)
    sphereSource.SetStartPhi(10.0)
    sphereSource.SetEndPhi(170.0)
    sphereSource.SetThetaResolution(8)

    # Inscribe some values onto the points(vertices)
    valueMaker = vtk.vtkRandomAttributeGenerator()
    valueMaker.SetInputConnection(sphereSource.GetOutputPort())
    valueMaker.SetGeneratePointScalars(1)
    valueMaker.SetGeneratePointVectors(1)

    # Average those onto the cells(triangles)
    point2cell = vtk.vtkPointDataToCellData()
    point2cell.SetInputConnection(valueMaker.GetOutputPort())
    point2cell.SetPassPointData(1)

    # Place the result in the visible scene
    # mapper to make openGL calls
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputConnection(point2cell.GetOutputPort())

    # actor to place the object into the scene
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetInterpolationToFlat()

    # a window on the desktop and a region of pixels within it
    renderer = vtk.vtkRenderer()
    renderWindow = vtk.vtkRenderWindow()
    renderWindow.AddRenderer(renderer)

    # An interactor to catch system and user events
    renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)

    # Catch the 'u' key and do something with it
    global PointToCell
    PointToCell = point2cell
    renderWindowInteractor.AddObserver("UserEvent", UserCallback)

    # Add the actor to the scene
    renderer.AddActor(actor)

    # Make a widget to interact with
    # modern widgets have independent display ...
    sliderRep = vtk.vtkSliderRepresentation3D()
    sliderRep.SetMinimumValue(3.0)
    sliderRep.SetMaximumValue(50.0)
    sliderRep.SetValue(sphereSource.GetThetaResolution())
    sliderRep.SetTitleText("Sphere Resolution")
    sliderRep.GetPoint1Coordinate().SetCoordinateSystemToWorld()
    sliderRep.GetPoint1Coordinate().SetValue(-4,6,0)
    sliderRep.GetPoint2Coordinate().SetCoordinateSystemToWorld()
    sliderRep.GetPoint2Coordinate().SetValue(4,6,0)
    sliderRep.SetSliderLength(0.075)
    sliderRep.SetSliderWidth(0.05)
    sliderRep.SetEndCapLength(0.05)
    # ... and interaction components
    sliderWidget = vtk.vtkSliderWidget()
    sliderWidget.SetInteractor(renderWindowInteractor)
    sliderWidget.SetRepresentation(sliderRep)
    sliderWidget.SetAnimationModeToAnimate()
    sliderWidget.EnabledOn()

    # Observe the widget's slider events too
    global SphereSource
    SphereSource = sphereSource
    sliderWidget.AddObserver("InteractionEvent", vtkSliderCallback)

    # Start up the application event loop
    renderWindow.Render()
    renderWindowInteractor.Start()

main()
